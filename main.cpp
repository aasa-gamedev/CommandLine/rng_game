#include <iostream>
#include "Game/Game.h"

int main() {
    std::cout << "Welcome to the RNG game." << std::endl;
    std::cout << "It is known that the numbers are sorted lower -> larger. The numbers may repeat." << std::endl;
    std::cout << "Enter the PIN in the format of '1234': " << std::endl;

    Game game;
    game.start();

    std::cout << "Created by Alar Aasa. 2020" << std::endl;
    return 0;
}
