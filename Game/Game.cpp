//
// Created by alar on 29/11/2020.
//

#include <iostream>
#include "Game.h"

void Game::start() {
    gameState.init();
    while (true) {
        gameState.nextLevel();
        std::string enteredPin;
        if (!gameState.checkInput()) {
            std::cout << "Oops! You entered the wrong PIN. You made it to level " << gameState.level << "Type 'y' to start again." << std::endl;
            char tryAgain;
            std::cout << "START AGAIN? >> ";
            std::cin >> tryAgain;

            if (tryAgain != 'y') {
                break;
            }

            gameState.init();
        }
    }
}
