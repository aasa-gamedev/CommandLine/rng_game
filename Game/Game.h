//
// Created by alar on 29/11/2020.
//

#ifndef RNG_GAME_GAME_H
#define RNG_GAME_GAME_H


#include "GameState.h"

class Game {
public:
    void start();
private:
    GameState gameState;
};


#endif //RNG_GAME_GAME_H
