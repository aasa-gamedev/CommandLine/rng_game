//
// Created by alar on 29/11/2020.
//

#ifndef RNG_GAME_GAMESTATE_H
#define RNG_GAME_GAMESTATE_H

#include <vector>
#include <string>

class GameState {
public:
    int level;
    std::vector<int> generatedNumbers;

    void init();
    void nextLevel();
    bool checkInput();

private:
    static std::vector<int> generateNumbers(int amount, int max);
    bool checkNumbers(const std::string& input);

};


#endif //RNG_GAME_GAMESTATE_H
