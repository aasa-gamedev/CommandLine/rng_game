//
// Created by alar on 29/11/2020.
//

#include "GameState.h"
#include <random>
#include <iostream>
#include <string>
void GameState::init() {
    level = 0;
}

void GameState::nextLevel() {
    level++;
    int numbersSum = 0, numbersMultiply = 1;
    std::string numbersString;
    int amountToGenerate, maxNumber = 3;
    if (level <= 3) {
        maxNumber++;
        amountToGenerate = 3;
    } else if (level <= 6) {
        maxNumber++;
        amountToGenerate = 4;
    } else {
        maxNumber++;
        amountToGenerate = 5;
    }

    if (3+level > 10) {
        maxNumber = 9;
    }

    generatedNumbers = generateNumbers(amountToGenerate, maxNumber);

    int i = 0;
    for (const int value: generatedNumbers) {
        i++;
        numbersSum += value;
        numbersMultiply = numbersMultiply * value;
        if (i == generatedNumbers.size()) {
            numbersString += std::to_string(value);
        } else {
            numbersString += std::to_string(value) + ", ";
        }
    }

    std::cout << "You arrive on level " + std::to_string(level) << std::endl;
    std::cout << "The PIN has " << std::to_string(amountToGenerate) << " numbers." << std::endl;
    std::cout << "When adding all the numbers of the PIN, the result is " << std::to_string(numbersSum) << "." << std::endl;
    std::cout << "When multiplying all the numbers of the PIN, the result is " << std::to_string(numbersMultiply) << "." << std::endl;
    std::cout << "[" << numbersString << "]" << std::endl;
}

std::vector<int> GameState::generateNumbers(const int amount, int max) {

    std::vector<int> generated;
    generated.reserve(amount);

    if (max > 10) max = 9;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(1, max);

    for (int i=0; i < amount; i++) {
        generated.push_back(dist(mt));
    }

    std::sort(generated.begin(), generated.end());
    return generated;
}

bool GameState::checkNumbers(const std::string& input) {
    std::vector<char> inputSplit(input.begin(), input.end());
    if (generatedNumbers.size() != inputSplit.size()) {
        return false;
    }

    for (int i=0; i < inputSplit.size(); i++) {
        if (generatedNumbers[i] != inputSplit[i] - '0') {
            return false;
        }
    }
    return true;
}

bool GameState::checkInput() {
    std::string userInput;
    std::cout << "PIN >>   ";

    std::cin >> userInput;

    if (!std::all_of(userInput.begin(), userInput.end(), ::isdigit)) {
        return false;
    }
    return checkNumbers(userInput);
}
