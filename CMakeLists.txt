cmake_minimum_required(VERSION 3.17)
project(RNG_Game)

set(CMAKE_CXX_STANDARD 20)

add_executable(RNG_Game main.cpp Game/GameState.cpp Game/GameState.h Game/Game.cpp Game/Game.h)